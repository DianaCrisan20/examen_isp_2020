/*
        U     interface
        I - J dependency
        I - K unidirectional association
        N - I unidirectional association
        K - L composition
        K - S aggregation
 */


class I implements U{
    private long t;
    K k;

    public void f(){
        System.out.println("Method f from class I");
    }

    public void i(J j){
        System.out.println("Method i from class I, " +
                "which needs an object from " + j.getClass());
    }

    public void setK(K k) {
        this.k = k;
    }
}

class J{
}

class K{
    L l;
    S s;

    K(){
        l = new L();
    }

    public void setS(S s) {
        this.s = s;
    }
}

class L{
    public void metA(){
        System.out.println("Method A from class L");
    }
}

class N{
    I i;

    public void setI(I i) {
        this.i = i;
    }
}

class S{
    public void metB(){
        System.out.println("Method B from class S");
    }
}

interface U{
}

class Main{
    public static void main(String[] args) {
        J j = new J();

        L l = new L();
        l.metA();

        S s = new S();
        s.metB();

        K k = new K();
        k.setS(s);

        I i = new I();
        i.f();
        i.i(j);
        i.setK(k);

        N n = new N();
        n.setI(i);
    }
}

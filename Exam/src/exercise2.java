/*
        three text fields, one button
        the third text field should not be editable

        when clicking the button, the sum of the number of characters
        input in the first two text fields is written in the third
        text field
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

class AppFrame implements ActionListener{

    // the elements
    JFrame frame = new JFrame();
    JButton button = new JButton("PRESS HERE");
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();
    JTextField textField3 = new JTextField();

    // settings for the elements
    public AppFrame(){
        textField1.setBounds(100, 100, 200, 50);
        textField2.setBounds(100, 200, 200, 50);
        textField3.setBounds(100, 300, 200, 50);

        // third text field is read only
        textField3.setEditable(false);

        button.setBounds(120, 400, 160, 50);
        button.setFocusable(false);
        button.addActionListener(this);

        frame.add(button);
        frame.add(textField1);
        frame.add(textField2);
        frame.add(textField3);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500, 500);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    // the action made by the button
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button){
            String content1 = textField1.getText();
            String content2 = textField2.getText();

            int sumChars = content1.length() + content2.length();
            textField3.setText(String.valueOf(sumChars));
        }
    }
}

class MainClass{
    public static void main(String[] args) {
        AppFrame appFrame = new AppFrame();
    }
}
